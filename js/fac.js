/**
 * @file
 * Fast Autocomplete scripts.
 */

(function ($, Drupal, once) {
  'use strict';

  /**
   * Enables Fast Autocomplete functionality.
   */
  Drupal.behaviors.fac = {
    attach: function (context, settings) {
      $.each(drupalSettings.fac, function (index, value) {
        $(once('fac', value.inputSelectors)).fastAutocomplete(value);
      });
    }
  };

})(jQuery, Drupal, once);
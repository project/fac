<?php

namespace Drupal\fac\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file for Fast Autocomplete.
 * @package Drupal\fac\Commands
 */
class FacCommands extends DrushCommands {

  /**
   * The fac config storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $facConfigStorage;

  /**
   * FacCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->facConfigStorage = $entity_type_manager->getStorage('fac_config');
  }

  /**
   * Delete JSON files for all Fast autocomplete configurations.
   *
   * @option fac_config_ids
   *   Delete the JSON files of specific FacConfig entities.
   *
   * @usage drush fac:cache-clear
   *   Clears the JSON files of all FacConfig entities.
   * @usage drush fac:cache-clear --fac_config_ids=default,test
   *   Clears only the JSON files for 'default' and 'test'.
   *
   * @command fac:cache-clear
   * @aliases fac:cc
   */
  public function cacheClear($options = ['fac_config_ids' => NULL]) {
    $configurations_ids = NULL;
    if (strlen($options['fac_config_ids']) > 0) {
      $configurations_ids = array_map('trim', array_filter(explode(',', $options['fac_config_ids'])));
    }

    $configurations = $this->facConfigStorage->loadMultiple($configurations_ids);

    foreach ($configurations as $configuration) {
      $configuration->deleteFiles();

      $this->logger()
        ->success(dt('Fast Autocomplete json files for %label have been deleted.', [
          '%label' => $configuration->label(),
        ]));
    }

    if (!$configurations) {
      $this->logger()->notice(dt('No Fast Autocomplete configurations found'));
    }
  }

}
